## **User Location Weather API - Mule 4**

> This API retrieves a user location's current temperature in degree Celsius. Current weather information is retrieved from Openweathermap API.

------

## **API Design**

The API retrieves current weather information from [Openweather API](https://openweathermap.org/current). The Openweather API is not directly accessed by this API's implementation. Instead, the Openweather API is protected by a Mule Proxy which makes it more secure by individual policy application, provides independent scalability, acts as a system API layer and gets itself added to application network.

- The Mule Application source code is available at: [https://bitbucket.org/karthi2322/weather-api/src/master/](https://bitbucket.org/karthi2322/weather-api/src/master/)
- The Mule Proxy source code is available at: [https://bitbucket.org/karthi2322/openweathermap-proxy/src/master/](https://bitbucket.org/karthi2322/openweathermap-proxy/src/master/)

![Weather_API.png](Weather_API.png)

## **How to Install**

Use below snippet to Install on Cloudhub. Replace with your Anypoint credentials.

```
mvn clean -DskipTests package deploy -DmuleDeploy -P cloudhub -Dmule.version="4.3.0" -Dmule.env=DEV -Danypoint.username=<your Anypoint User> -Danypoint.password=<your Anypoint Password> -Dmule.env.uri=dev -Dmule.applicationName=user-location-weather-api -Dmule.environment=<Your Anypoint Environment Name> -Dch.businessGroup=<Your Anypoint Business Group> -Dch.region=<Your Anypoint AWS Region> -Dch.workers=<Provide no. of workers> -Dch.workerType=<Provide worker size>
```


## **API Security**

This API is protected using client id policy. Credentials can be requested from Exchange.

## **API Request Fields**

The API accepts requests in a JSON Format consisting of two keys, Header and Items. Both Header and Items keys have data in JSON String array format.

The Header Array denotes what information does the Item Array contain.

Below  are the information passed in the request items:

| Field    | Description   |
|----------|---------------|
| name |  The first name of the user |
| lastName |    The surname of the user   |
| dateTime | Date and time stamp of user's location in ddMMyyyyTHHmmX format |
| city | City of the User |
| country | Country of the User |

## **API Response Fields**

The API returns the response in a JSON Object format. The fields returned are described below:

| Field    | Description   |
|----------|---------------|
| lastname |  The surname of the user |
| name |    The firstname of the user   |
| timeZone | Time Zone of User's Location in GMT±hh:mm format (Derived from Openweather API response timezone offset based on City) |
| offset | Time Zone offset of User's Location in ±hh:mm format (Derived from Openweather API response timezone offset based on City) |
| fullName | Full name of the user derived by concatenating firstname and lastname with a space in between |
| temperatureCelcius | Current temperature in degree Celsius of the user's location |
| dateTime |  Date and time stamp of user's location plus 1 day in ISO 8601 yyyy-MM-ddTHH:mm:ss format (Derived from request Date Time)|
| city |  City of the User |
| location |  Country of the User |
